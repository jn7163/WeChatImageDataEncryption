﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeChatImageDatEncryption
{
    public class EncryptionUtil
    {
        public static byte[] EnDeFile(byte[] filebuffer, int key) {
            byte[] newBuffer = new byte[filebuffer.Length];
            for (int i = 0; i < filebuffer.Length; i++)
            {
                newBuffer[i] = (byte)(filebuffer[i] ^ key);
            }

            return newBuffer;
        }
    }
}
